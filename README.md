# pi_school_demo_web

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Getting Twilio KEYS for .env
- Visit and register https://www.twilio.com/try-twilio
- Create a project
- Configure the room 
  - Max participants 2 for room size (will be free usage)
  - More than 2 participants (will be paid) Most likely you can get 10$ free trial for development test propose;
- Copy your keys.

### Points for update app
 - Detect dominantSpeaker - reem twilio docs
 - Detect availability of mic|camera before calling createLocal tracks
 - Add Data track (for chat || emoji)
 - Add ScreenSharing  