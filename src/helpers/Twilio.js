const {jwt: {AccessToken}} = require('twilio');

const {VideoGrant} = AccessToken;

export default function generateToken(userName = 'John Smith') {
    //Generate Token using data from .env file (Read Readme.md for more info)
    const token = new AccessToken(
        process.env.VUE_APP_TWILIO_ACCOUNT_SID,
        process.env.VUE_APP_TWILIO_API_KEY,
        process.env.VUE_APP_TWILIO_API_SECRET,
        {
            ttl: 14400,
        },
    );
    
    token.identity = userName;
    const grant = new VideoGrant();
    token.addGrant(grant);
    return token.toJwt();
}
